window.requestAnimFrame = (function(callback) {
	return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
		function(callback) {
			window.setTimeout(callback, 1000 / 60);
        };
})();

function drawText(person, context) {
	console.log("Drawing text for person:"+person);
	var p = myText.p[person];
	context.fillStyle = myText.fillStyle;
    context.font = myText.font;
	context.fillText(p.name, myText.x, myText.y);
	context.fillText(p.role, myText.x, myText.y+myText.lineHeight);
	context.fillText(p.thesis, myText.x, myText.y+myText.lineHeight*2);
}

function animate(person, canvas, context, startTime) {
	// update
    var time = (new Date()).getTime() - startTime;

    var linearSpeed = 1000;
    // pixels / second
    var newX = canvas.width - (linearSpeed * time / 1000);
    if(newX > canvas.width/1.4) {
		myText.x = newX;
    }else return;
	
	//change font size if text does not fit
	var curFontSize = (myText.font.split("px")[0]).split(' ')[1];
	context.font = curFontSize;
	var fontWidth = Math.round(context.measureText(myText.p[person].thesis).width);
	while (((canvas.width/1.4) + fontWidth) > (canvas.width-10)){
		newFontSize = curFontSize-1;
		myText.font = "bold "+newFontSize+"px roboto";
		curFontSize = newFontSize;
		context.font = myText.font;
		fontWidth = context.measureText(myText.p[person].thesis).width;
	}

    // clear
    clearCanvas(canvas,context);

	drawText(person,context);

    // request new frame
    requestAnimFrame(function() {
		animate(person, canvas, context, startTime);
    });
}

function clearCanvas(canvas,context){
	context.clearRect(0, 0, canvas.width, canvas.height);
}

var text_canvas = {
	canvas	: document.getElementById('text_canvas'),
	ctx		: document.getElementById('text_canvas').getContext('2d')
}

var personsEN = [
	{
		name		: "Dimitris Koudounakis",
		role		: "Architect Engineer",
		thesis		: "General - Design Manager"
	},
	{
		name		: "Giorgos Inglezakis",
		role		: "Architect Engineer",
		thesis		: "General - Construction Manager"
	},
	{
		name		: "Angeliki Christaki",
		role		: "Architect Engineer",
		thesis		: "Design Manager"
	},
	{
		name		: "Aris Tzedakis",
		role		: "Civil Engineer",
		thesis		: "Structural design - Sales Manager"
	},
	{
		name		: "Nikos Batakis",
		role		: "Architect Engineer",
		thesis		: "Construction Supervisor"
	},
	{
		name		: "Vasia Katramadaki",
		role		: "Architect Engineer",
		thesis		: "Design"
	},
	{
		name		: "Christiana Karfi",
		role		: "Architect Engineer",
		thesis		: "Design"
	},
	{
		name		: "Chrisa Tsatsaroni",
		role		: "Architect Engineer",
		thesis		: "Graphic Design"
	},
	{
		name		: "Manolis Levedelis",
		role		: "Film Director - Editor",
		thesis		: "Photography & Visualization"
	},
	/*{
		name		: "Spiros Papaspurou",
		role		: "Marketing & Branding",
		thesis		: "Sales Manager - Europe"
	},*/
	{
		name		: "Mitsos",
		role		: "Rottweiler",
		thesis		: "Security"
	}
];
var personsEL = [
	{
		name		: "Δημήτρης Κουδουνάκης",
		role		: "Αρχιτέκτων Μηχανικός",
		thesis		: "General - Design Manager"
	},
	{
		name		: "Γιώργος Ιγγλεζάκης",
		role		: "Αρχιτέκτων Μηχανικός",
		thesis		: "General - Construction Manager"
	},
	{
		name		: "Αγγελική Χριστάκη",
		role		: "Αρχιτέκτων Μηχανικός",
		thesis		: "Design Manager"
	},
	{
		name		: "Άρης Τζεδάκης",
		role		: "Πολιτικός Μηχανικός",
		thesis		: "Structural design - Sales Manager"
	},
	{
		name		: "Νίκος Μπατάκης",
		role		: "Αρχιτέκτων Μηχανικός",
		thesis		: "Construction Supervisor"
	},
	{
		name		: "Βάσια Κατραμαδάκη",
		role		: "Αρχιτέκτων Μηχανικός",
		thesis		: "Design"
	},
	{
		name		: "Χριστιάνα Καρφή",
		role		: "Αρχιτέκτων Μηχανικός",
		thesis		: "Design"
	},
	{
		name		: "Χρύσα Τσατσαρώνη",
		role		: "Αρχιτέκτων Μηχανικός",
		thesis		: "Graphic Design"
	},
	{
		name		: "Μανώλης Λεβεντέλης",
		role		: "Film Director - Editor",
		thesis		: "Photography & Visualization"
	},
	/*{
		name		: "Σπύρος Παπασπύρου",
		role		: "Marketing & Branding",
		thesis		: "Sales Manager - Europe"
	},*/
	{
		name		: "Μήτσος",
		role		: "Rottweiler",
		thesis		: "Security"
	}
];
var myCurLang = 'en'; //window.parent.document.getElementById("JSLang").innerHTML;
var persons = (myCurLang=="el") ? personsEL : personsEN;

function indexOfPerson(p){
	if (!p) return -1;
	console.log("p="+p);
	for (i=0;i<persons.length;i++){
		if (p=="mitsos" || p=="Μήτσος") return persons.length-1;
		else{
			var url = persons[i].name.split(' ')[1].toLowerCase();
			if (url==p) return i;
		}
	}
	return -1;
}

var myText = {
	x			: 0,
    y			: 0, // calculated inside pixel_slider init
	lineHeight	: 25,
	fillStyle	: "#8ED6FF",
	font 		: "bold 28px roboto",
	p			: persons,
	indexof		: indexOfPerson
};