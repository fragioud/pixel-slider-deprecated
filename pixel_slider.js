var pixel_slider = {
	canvas 	: document.getElementById("image_canvas"),
	ctx 	: document.getElementById("image_canvas").getContext("2d"),
	width	: 0,
	height	: 0,
	
	/* BEGIN user defined */
	pixel_size		: 120,      // auto calculated
	num_of_pixels	: 10,
	step			: 1,		// unimplemented
	time_per_image	: 5, 		// scrolls
	loop			: false, 	// unimplemented
	/* END user defined */
	
	images					: getImages(),
	pixels					: [],
	actual_num_of_pixels	: 10, // auto calculated
	
	state		: 'DOWN', // UP, DOWN
	hold		: false,
	image_index : 1,
	pixel_index : -1,
	last_state	: 'DOWN',
	timer		: 0,
	
	reset : function(image){
		this.image_index 	= image!=-1 ? image : 1;
		this.last_state		= "DOWN";
		this.pixel_index	= image!=-1 ? this.actual_num_of_pixels+1 : -1;
		this.timer			= this.time_per_image;
		this.pixels			= [];
		this.hold			= false;
		myText.font			= "bold 28px roboto";
	},
	
	init : function(){
		console.log("Initializing slider...");
		
		var p = myText.indexof(getURLParameter('p'));
		console.log(p);
		this.reset(p);
		
		this.canvas.width  = this.width = window.parent.document.body.clientWidth   -14; // -2 for the border
		this.canvas.height = this.height = window.parent.document.getElementsByClassName('content')[0].clientHeight *0.9;
		console.log("Width:"+this.width+" Height:"+this.height);
		window.parent.document.getElementById("slider-iframe").width = this.width;
		window.parent.document.getElementById("slider-iframe").height = this.height;
		
		window.parent.document.getElementsByClassName('content')[0].style.minHeight = 'auto';
		
		fitImageOn(this.canvas,this.images[p!=-1?p:0]);
		
		this._calculate_pixel_size();
		
		for (i=0; i<renderableWidth; i+=this.pixel_size){
			for (j=0; j<renderableHeight; j+=this.pixel_size){
				this.pixels.push({'x':i,'y':j});
			}
		}
		this.actual_num_of_pixels = this.pixels.length;
		this.pixels = shuffle(this.pixels);
		console.log("Slider initialized. Pixel Size="+this.pixel_size+" Num of pixels="+this.actual_num_of_pixels+" pixels length="+this.pixels.length);
		
		//init text
		text_canvas.canvas.width = pixel_slider.canvas.width;
		text_canvas.canvas.height = pixel_slider.canvas.height;
		myText.x = text_canvas.canvas.width;
		myText.y = text_canvas.canvas.height/2;
		var startTime = (new Date()).getTime();
		animate(p!=-1?p:this.image_index-1, text_canvas.canvas, text_canvas.ctx, startTime);
	},
	
	rotate :function(){
		if (this.state == "UP"){
			if (this.state != this.last_state){
				this.decrease_image_index();
			}
			if (this.pixel_index == 0 && this.image_index > 0){ // image completed
				if (this.hold == true && this.timer == 0){
					this.decrease_image_index();
					this.pixel_index = this.actual_num_of_pixels-1;
					this.timer = this.time_per_image;
					this.hold = false;
					clearCanvas(text_canvas.canvas,text_canvas.ctx);
				}else{
					if (this.hold == false){
						var startTime = (new Date()).getTime();
						animate(this.image_index, text_canvas.canvas, text_canvas.ctx, startTime);
					}
					this.timer--;
					this.hold = true;
				}
				console.log("HOLD: "+this.hold+" Image Timer: "+this.timer);
			}else if( this.pixel_index == 0 && this.image_index == 0){ // first image completed
				// do nothing
			}else if (this.state != this.last_state){
				this.decrease_image_index();
			}
			else{
				this.decrease_pixel_index();
				clearCanvas(text_canvas.canvas,text_canvas.ctx);
			}
		}
		else{ // scroll down
			if (this.state != this.last_state){
				this.increase_image_index();
			}
			if (this.pixel_index == this.actual_num_of_pixels-1 && this.image_index < this.images.length){ // image completed
				if (this.hold == true && this.timer == this.time_per_image*2){
					console.log("Image Timer:"+this.timer);
					this.increase_image_index();
					this.pixel_index = 0;
					this.timer = this.time_per_image;
					this.hold = false;
					clearCanvas(text_canvas.canvas,text_canvas.ctx);
				}else{
					if (this.hold == false){
						var startTime = (new Date()).getTime();
						animate(this.image_index, text_canvas.canvas, text_canvas.ctx, startTime);
					}
					this.timer++;
					this.hold = true;
					console.log("Image Timer: "+this.timer);
				}
			}else if( this.pixel_index == this.actual_num_of_pixels-1 && this.image_index == this.images.length){ // last image completed
				// do nothing
			}else if (this.state != this.last_state){
				this.increase_image_index();
			}
			else{
				this.increase_pixel_index();
				clearCanvas(text_canvas.canvas,text_canvas.ctx);
			}
		}
	},
	
	increase_image_index : function(){
		(this.image_index < this.images.length-1) ? this.image_index++ : '';
	},
	decrease_image_index : function(){
		(this.image_index > 0) ? this.image_index-- : '';
	},
	increase_pixel_index : function(){
		(this.pixel_index < this.actual_num_of_pixels-1) ? this.pixel_index++ : this.increase_image_index();
	},
	decrease_pixel_index : function(){
		(this.pixel_index > 0) ? this.pixel_index-- : this.decrease_image_index();
	},
	
	_calculate_pixel_size : function(){
		/**/
		var emv = (renderableWidth * renderableHeight) / this.num_of_pixels;
		this.pixel_size = Math.round(Math.sqrt(emv));
		/**/
	}
};

var myInterval;
var workInProgress = false;
function nextPerson(){
	if (workInProgress) return;
	workInProgress = true;
	if (pixel_slider.image_index == pixel_slider.images.length){
		workInProgress = false;
		return;
	}
	myInterval = setInterval(function(){
		if (pixel_slider.pixel_index == pixel_slider.actual_num_of_pixels-1){
			clearInterval(myInterval);
			rotate({"deltaY":-1});
			var startTime = (new Date()).getTime();
			animate(pixel_slider.image_index, text_canvas.canvas, text_canvas.ctx, startTime);
			pixel_slider.timer = pixel_slider.time_per_image;
			pixel_slider.hold = false;
			pixel_slider.image_index++;
			pixel_slider.pixel_index = -1;
			workInProgress = false;
			return;
		}
		rotate({"deltaY":-1});
	}, 100);
}

function previousPerson(){
	if (workInProgress) return;
	workInProgress = true;
	if (pixel_slider.image_index == 0){
		workInProgress = false;
		return;
	}
	pixel_slider.image_index--;
	if (pixel_slider.last_state == "DOWN"){
		pixel_slider.pixel_index = pixel_slider.actual_num_of_pixels-1;
	}else{
		pixel_slider.pixel_index = pixel_slider.actual_num_of_pixels;
	}
	console.log(pixel_slider.last_state+"!!!!"+pixel_slider.pixel_index);
	pixel_slider.timer = 6;
	pixel_slider.hold = false;
	myInterval = setInterval(function(){
		if (pixel_slider.pixel_index == 0){
			clearInterval(myInterval);
			var startTime = (new Date()).getTime();
			animate(pixel_slider.image_index, text_canvas.canvas, text_canvas.ctx, startTime);
			workInProgress = false;
			return;
		}
		rotate({"deltaY":1});
	}, 100);
}

function handleResize(){
	pixel_slider.init(getURLParameter('p'));
}

console.log("person:"+getURLParameter('p'));
window.onload = function(){
	pixel_slider.init(getURLParameter('p'));
	
	if (isMobileDevice()){
		$(function() {
			$("#text_canvas").swipe( {
			  //Single swipe handler for left swipes
			  swipeLeft:function(event, direction, distance, duration, fingerCount) {
			   nextPerson();
			  },
			  swipeRight:function(event, direction, distance, duration, fingerCount) {
			   previousPerson();
			  },
			  threshold:75
			});
		});
	}
	else{
		jQuery("#text_canvas").mousewheel(rotate);
	}
	
	window.parent.window.addEventListener("resize", handleResize);
}